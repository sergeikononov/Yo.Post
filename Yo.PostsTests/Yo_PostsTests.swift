//
//  Yo_PostsTests.swift
//  Yo.PostsTests
//
//  Created by Сергей on 06/03/2019.
//  Copyright © 2019 Yo. All rights reserved.
//

import XCTest
@testable import Yo_Posts

class Yo_PostsTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testGetPosts() {
        let sut = ListTableViewController()
        sut.getPosts()
        NetworkManager.sharedInstance.getPostList(success: { item in
            guard let posts = item else {
                XCTAssertNil(item)
                return
            }
            guard let sutPosts = sut.post else {
                return
            }
            XCTAssertEqual(sutPosts.first?.data.count, posts.first?.data.count)
        }) { (err) in
            XCTAssertEqual(err, "Нет данных")
        }
        XCTAssertNotNil(sut.post)
        
    }

}
