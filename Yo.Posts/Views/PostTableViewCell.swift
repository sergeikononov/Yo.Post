//
//  PostTableViewCell.swift
//  Yo.Posts
//
//  Created by Sergei on 04/03/2019.
//  Copyright © 2019 Yo. All rights reserved.
//

import UIKit

class PostTableViewCell: UITableViewCell {
    @IBOutlet weak var postLabel: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var time: UILabel!
    
    func setupCell(objectForCell: Detail) {
        postLabel.text = objectForCell.text
        userName.text = objectForCell.ownerName
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let timeFormat = formatter.string(from: Date(timeIntervalSince1970: Double((objectForCell.date)) / 1000.0))
        time.text = timeFormat
        if objectForCell.attachments != "" {
            avatarImage.downloadedFrom(link: objectForCell.attachments)
        } else {
            avatarImage.image = nil
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
