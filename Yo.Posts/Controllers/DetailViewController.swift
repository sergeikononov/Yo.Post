//
//  DetailViewController.swift
//  Yo.Posts
//
//  Created by Sergei on 05/03/2019.
//  Copyright © 2019 Yo. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timestampLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var dislikes: UILabel!
    @IBOutlet weak var likes: UILabel!
    @IBOutlet weak var numberOfViewsLabel: UILabel!
    @IBOutlet weak var stackToImage: NSLayoutConstraint!
    @IBOutlet weak var stackToText: NSLayoutConstraint!
    
    var detailPost: Detail!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView(postObject: detailPost)
    }
    
    func setupView(postObject: Detail) {
        
        if postObject.attachments != "" {
            avatarImage.downloadedFrom(link: postObject.attachments)
        } else {
            avatarImage.isHidden = true
        }
        
        nameLabel.text = postObject.ownerName
        textLabel.text = postObject.text
        var formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        timestampLabel.text = formatter.string(from: Date(timeIntervalSince1970: Double(detailPost.date) / 1000.0))
        postImage.downloadedFrom(link: postObject.ownerPhoto)
        dislikes.text = String(postObject.dislikes)
        likes.text = String(postObject.likes)
        numberOfViewsLabel.text = String(postObject.postviews)
    }
    
    

}
