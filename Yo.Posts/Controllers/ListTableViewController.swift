//
//  ListTableViewController.swift
//  Yo.Posts
//
//  Created by Sergei on 04/03/2019.
//  Copyright © 2019 Yo. All rights reserved.
//

import UIKit
import SystemConfiguration
import SwiftyJSON

class ListTableViewController: UITableViewController {
    
    var post: [Post]! = []

    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl?.attributedTitle = NSAttributedString(string: "Обновление постов")
        refreshControl?.addTarget(self, action: #selector(self.refresh), for: .valueChanged)
        getPosts()
    }
    
    func getPosts() {
        if isInternetAvailable() {
            NetworkManager.sharedInstance.getPostList(success: { (item) in
                self.post = item
                self.tableView.reloadData()
            }) { err in
                showAlertWithOk(self, title: "Предупреждение", message: err)
                self.post = Post.fromJSON(json: [JSON(UserDefaults.standard.object(forKey: "posts") as Any)])
                self.tableView.reloadData()
            }
        } else {
            showAlertWithOk(self, title: "Предупреждение", message: "Нет интернет соединения, показываю предыдущие посты")
            self.post = Post.fromJSON(json: [JSON(UserDefaults.standard.object(forKey: "posts") as Any)])
            self.tableView.reloadData()
            self.refreshControl?.endRefreshing()
        }
    }
    
    @objc func refresh(sender: AnyObject) {
        getPosts()
        refreshControl?.endRefreshing()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if (post?.isEmpty)! {
            return 0
        } else {
            return (self.post?.first?.data.count)!
        }
        
    }
    
    // Проверка есть ли соединение с интернетом
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "post", for: indexPath) as? PostTableViewCell else {
            return UITableViewCell()
        }
        
        guard let post = self.post.first?.data else {
            return UITableViewCell()
        }
        
        cell.setupCell(objectForCell: post[indexPath.row])
        return cell
    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detail" {
            if let destination = segue.destination as? DetailViewController {
                if let indexPath = (tableView.indexPathForSelectedRow as IndexPath?) {
                    destination.detailPost = self.post?.first?.data[indexPath.row]
                }
            }
        }
    }

}
