//
//  Posts.swift
//  Yo.Posts
//
//  Created by Sergei on 04/03/2019.
//  Copyright © 2019 Yo. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit

struct Post {
    let error: Bool
    let code: Int
    let data: [Detail]
    
    static func fromJSON(json: [JSON]) -> [Post]? {
        return json.compactMap { jsonItem -> Post in
            let error = jsonItem["error"].boolValue
            let code = jsonItem["code"].intValue
            let data = jsonItem["data"].arrayValue
            
            return Post(error: error, code: code, data: Detail.fromJSON(json: data)!)
        }
    }
}

struct Detail {
    let id: Int
    let postviews: Int
    let text: String
    let lang: String
    let authorWatch, status, adult, hidden: Int
    let category, filter: Int
    let categoryString: String
    let type, openComments, date, reposts: Int
    let ownerName: String
    let ownerID: Int
    let ownerPhoto: String
    let likes, dislikes, comments: Int
    let attachments: String
    
    static func fromJSON(json: [JSON]) -> [Detail]? {
        return json.compactMap { jsonItem -> Detail in
            let id = jsonItem["id"].intValue
            let postviews = jsonItem["postviews"].dictionaryObject
            let text = jsonItem["text"].stringValue
            let lang = jsonItem["lang"].stringValue
            let authorWatch = jsonItem["author_watch"].intValue
            let status = jsonItem["status"].intValue
            let adult = jsonItem["adult"].intValue
            let hidden = jsonItem["hedden"].intValue
            let category = jsonItem["category"].intValue
            let filter = jsonItem["filter"].intValue
            let categoryString = jsonItem["category_string"].stringValue
            let type = jsonItem["type"].intValue
            let openComments = jsonItem["open_comments"].intValue
            let date = jsonItem["date"].intValue
            let reposts = jsonItem["reports"].intValue
            let ownerName = jsonItem["owner_name"].stringValue
            let ownerID = jsonItem["owner_id"].intValue
            let ownerPhoto = jsonItem["owner_photo"].stringValue
            let likes = jsonItem["likes"].dictionaryObject
            let dislikes = jsonItem["dislikes"].dictionaryObject
            let comments = jsonItem["comments"].dictionaryObject
            let temp = JSON(jsonItem["attachments"].arrayValue).first
            let attachments = temp?.1["photo"]["photo_medium"].string
            return Detail(id: id,
                          postviews: postviews!["count"] as! Int,
                          text: text,
                          lang: lang,
                          authorWatch: authorWatch,
                          status: status,
                          adult: adult,
                          hidden: hidden, category: category, filter: filter, categoryString: categoryString, type: type, openComments: openComments, date: date, reposts: reposts, ownerName: ownerName, ownerID: ownerID, ownerPhoto: ownerPhoto, likes: likes!["count"] as! Int, dislikes: dislikes!["count"] as! Int, comments: comments!["count"] as! Int, attachments: attachments ?? "")
        }
    }
}

struct PostView {
    let count: Int
    let my: Int
    
    static func fromJSON(json: [JSON]) -> [PostView]? {
        return json.compactMap { jsonItem -> PostView in
            let count = jsonItem["count"].intValue
            let my = jsonItem["my"].intValue
            
            return PostView(count: count, my: my)
        }
    }
}
