//
//  NetworkManager.swift
//  Yo.Posts
//
//  Created by Sergei on 04/03/2019.
//  Copyright © 2019 Yo. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

final class NetworkManager {
    
    class var sharedInstance: NetworkManager {
        struct SingletonWrapper {
            static let singleton = NetworkManager()
        }
        return SingletonWrapper.singleton
    }
    
    //Получение постов
    func getPostList(success: @escaping (_ value: [Post]?) -> Void = {_ in }, failure: @escaping (_ message: String) -> Void = {_ in }) {
        Alamofire.request("http://dev.apianon.ru:3000/posts/get", method: .post, encoding: JSONEncoding.default)
            .responseJSON { response in
                guard let data = response.data else {
                    failure("Нет данных")
                    return
                }
                let json = JSON(data as Any)
                // Не понял точно, что от меня надо было, но видимо нужно было сохранять ответ на запрос в каком-то виде. Хотел с core data, но не успел.
                // Вот грязное подобие "слой кэша"
                UserDefaults.standard.set(data, forKey: "posts")
                success(Post.fromJSON(json: [json]))
        }
    }
}
